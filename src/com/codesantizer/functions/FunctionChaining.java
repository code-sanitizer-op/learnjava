package com.codesantizer.functions;

import java.util.function.Function;

public class FunctionChaining {

    public static void main(String[] args) {

        Function<String, Integer> func = x -> x.length();
        Function<Integer, Integer> func2 = x -> x*2;
        Integer apply = func.andThen(func2).apply("code-sanitizer");

        System.out.println(apply);

    }
}
