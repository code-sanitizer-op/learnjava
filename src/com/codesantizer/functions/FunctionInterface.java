package com.codesantizer.functions;

import java.util.function.Function;

public class FunctionInterface {

    public static void main(String[] args) {

        Function<String, Integer> func = x -> x.length();

        Integer apply = func.apply("code-sanitizer");

        System.out.println(apply);

    }
}
